import React, { Component } from 'react';
import _ from 'lodash';


class CurrentSession extends Component {
  renderSpeakers(showBio) {
    return _.map(this.props.session.Speakers, (speaker) => {
      return (
        <div key={speaker.AttendeeID} className="speaker-name">
          <strong>{speaker.FirstName} {speaker.LastName}</strong>, {speaker.Company}
          <br /><br />
          {showBio ? speaker.Biography : ""}
        </div>
      );
    })
  };

  render() {
    let {session} = this.props;
    return (
      <article className="current-session">
        <h1>{session.Title}</h1>
        <span className="current-session-speakers">{this.renderSpeakers(false)}</span>
        <p>
          {session.Description}
        </p>
        <a href="/">See the Q&A from this talk and others here.</a>
        <h2>About the speaker{session.Speakers && session.Speakers.length > 1 ? "s" : " "}</h2>
        {this.renderSpeakers(true)}
      </article>
    )
  }
}

export default CurrentSession;
