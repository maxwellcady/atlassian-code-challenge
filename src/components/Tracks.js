import React, { Component } from 'react';
import _ from 'lodash';

class Tracks extends Component {
  renderTracks() {
    // Map through all tracks to display them.
    return _.map(this.props.tracks, (track) => {
      if (track.Title !== 'Activities' && track.Title !== 'Training') {
        return (
          <span
            key={track.Title}
            className={"tracks-tab " + (this.props.currentTrack.Title === track.Title ? "active" : "inactive")}
            onClick={() => this.props.handleClick(track)}
            >
            {track.Title}
          </span>
        )
      }
    });
  }
  render() {
    return (
      <nav className="tracks">
        {this.renderTracks()}
      </nav>
    );
  }
}

export default Tracks;
