import React from 'react';
import whiteLogo from './../images/Atlassian-horizontal-white.png';
import blueLogo from './../images/Atlassian-horizontal-blue.png';

const Header = () => {
  return (
    <header className="header">
      <div className="header-bar">
        <img src={whiteLogo} className="logo" alt="Atlassian"/>
      </div>
      <div className="header-main">
        <img src={blueLogo} alt="Atlassian Summit"/>
        <h1>Video Archive</h1>
      </div>
    </header>
  )
}

export default Header;
