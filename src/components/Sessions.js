import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import _ from 'lodash';

class Sessions extends Component {
  renderSessions() {

    // Map through all sessions to display them.
    return _.map(this.props.sessions, (session) => {
      return (
        <Link to={session.Id} key={session.Id}>

          <article
            className={"session " + (this.props.currentSession.Title === session.Title ? "active" : "inactive")}
            onClick={() => this.props.handleClick(session)}
            >
            <h4 className="session-title">{session.Title}</h4>
            <span className="session-speakers">
              {this.renderSpeakers(session.Speakers)}
            </span>
          </article>
        </Link>
      )
    });
  }
  renderSpeakers(speakers) {
    return _.map(speakers, (speaker) => {
      return <span key={speaker.AttendeeID}>{speaker.FirstName} {speaker.LastName}, {speaker.Company}<br /></span>
    })
  }
  render() {
    return (
      <div className="sessions">
        <h1>{this.props.currentTrack.Title}</h1>
        <span>{this.renderSessions()}</span>
      </div>
    );
  }
}

export default Sessions;
