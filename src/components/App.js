import React, { Component } from 'react';
import _ from 'lodash';
import sessions from '../sessions.json';
import Header from './Header';
import Tracks from './Tracks';
import Sessions from './Sessions';
import CurrentSession from './CurrentSession';
import '../styles/index.css';

class App extends Component {
  state = {
    tracks: [],
    sessions: [],
    currentTrack: null,
    currentSessions: [], // Sessions from selected track
    currentSession: null
  }
  componentWillMount() {
    let tracks = [];

    // Convert sessions object to array
    const sessionsArr = Object.values(sessions);

    // Loop through session to retrieve track titles
    sessionsArr[0].forEach(session => {
      tracks.push(session.Track);
    });

    // Removes duplicates from array with the help of lodash.
    tracks = _.uniqBy(tracks, 'Title');

    // If router params contain an ID, set session with that ID to active session
    if (this.props.match) {
      console.log(this.props.match.params);
      const foundSession = _.filter(sessionsArr[0], (session) => {
        return session.Id === this.props.match.params.id;
      })[0];
      this.setState({
        tracks: tracks,
        sessions: sessionsArr[0],
        currentSession: foundSession,
      }, this.setCurrentTrack(foundSession.Track, false));
    } else {
      this.setState({
        tracks: tracks,
        sessions: sessionsArr[0],
        currentTrack: tracks[0],
        currentSession: sessionsArr[0][0],
      }, this.setCurrentTrack(tracks[0], true)) // Callback to automatically select first track
    }
  }

  setCurrentTrack(track, setSession) {
    this.setState({currentTrack: track}, () => {
      const currentTrackSessions = _.filter(this.state.sessions, (session) => {
        return session.Track.Title === this.state.currentTrack.Title;
      });
        this.setState({currentSessions: currentTrackSessions}, () => {
          if (setSession) {
            this.setState({currentSession: currentTrackSessions[0]})
          }
        })
    })
  }

  setCurrentSession(session) {
    this.setState({currentSession: session});
  }

  render() {
    return (
      <div className="App">
        <Header />
        <main className="main">
          <Tracks
            tracks={this.state.tracks}
            currentTrack={this.state.currentTrack}
            handleClick={track => this.setCurrentTrack(track, true)}
          />
          <div className="main-content">
            <Sessions
              sessions={this.state.currentSessions}
              currentTrack={this.state.currentTrack}
              currentSession={this.state.currentSession}
              handleClick={session => this.setCurrentSession(session)}
            />
            <CurrentSession
              session={this.state.currentSession}
            />
          </div>
        </main>
      </div>
    );
  }
}

export default App;
