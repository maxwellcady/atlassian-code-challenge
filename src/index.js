import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter, Route } from 'react-router-dom';
import App from './components/App';

import './styles/index.scss';

ReactDOM.render((
    <BrowserRouter>
      <div>
        <Route path="/" exact render={() => <App/>} />
        <Route path="/:id" render={(props) => <App {...props} />} />
      </div>
    </BrowserRouter>
  ), document.getElementById('root'));
