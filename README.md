# Atlassian Code Challenge - Summit

Built using React, bootstrapped via create-react-app. The project uses React Router to dynamically update currently shown session based on route.

# Tools used
* React
* React-Router
* Sass
* Lodash

# Instructions
This project uses npm-run-all to execute scripts simultaneously. To install:

`npm install -g npm-run-all`

Then install dependencies via `npm install`

You can then start a live dev server by running `npm start`. This will navigate you to localhost:3000

The app can be built for production using `npm run build`
